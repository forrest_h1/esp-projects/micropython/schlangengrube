# Schlangengrube



Schlangengrube, or, Schlangen for short, is a Rogue SSID/AP Scanner designed for the ESP32 and written in MicroPython (v1.19.1 at the time of this readme). Due to the potential for abuse and me pretending this is an educational tool, you'll need to manually add your own convincing SSIDs. 



## How It Works



I've added a form of MAC/BSSID Randomization that seems to be working, but it is VERY lazy. I haven't figured out exactly what the criteria is for MAC spoofing on the ESP32, so there is no MAC generation per SSID like I would have normally done this - instead, there are 5 MACs I've generated with my tool, GreenTree, that just  happened to work. 



Additionally, THIS DOES NOT RUN ON BOOT. I feel like that would have been a pretty terrible idea, so you'll need to manually run the script. Surely, if you're enough of a dork to be looking for an obscure AP Spawner, you'll be able to figure out how to make this run on boot :) 


## Changes Planned


- True MAC randomization per SSID Broadcasted


- Run-on-button-press implementation
