#Schlangren - Rogue AP Spawner using the ESP32
#
#This was explicitly written for the ESP32s I got for Christmas - I only have the Amazon Espressif clones to test with right now.
#Some changes might need to be made for your devices.
#
#Written by Forrest Hooker, 04/11/2023

from time import sleep
import network
import machine 
import random
import ubinascii

#Set the LED Pin (Might be different on your device!)
LED = machine.Pin(2, machine.Pin.OUT)

## IMPORTANT ##
#In order to set the BSSID, you have to define the 'mac' value as a HEX version of the string.
#WITHOUT the octet division. So if we give it a list of MACs, we either have to remove the ":" or "-"
#Or we don't use them at all and only add them during printing.

#This was done VERY sloppily because I'm not sure what the ESP uses as criteria for validating a MAC.
bssidList = ["547FA75EC3A1", "543DDF183708", "FE2DCC535827", "00032963b5c6"]

#Set a list of ESSID Names (Change these to whatever you'd like.)
ssidList = ["Test AP 1",
            "Test AP 2",
            "Test AP 3",
            "Test AP 4",
            "Test AP 5",
            "Test AP 6"]

#This part doesn't matter since we're just spamming beacon frames and not really waiting for a connection
#But I was paranoid :)
password = ("MEIN LEIBEN")


def __ssidSpawn__(ssid,bssid):
    #Define AP as the WLAN iFace in ACCESS POINT state
    ap = network.WLAN(network.AP_IF)
    #Enable AP 
    ap.active(True)
    #Configure the ESSID and BSSID(mac) as the ssid + bssid values fed as arguments
    ap.config(essid=ssid,mac=bssid)
    #Set authmode to 0 (I think this just means open?) and channel to 1 (Would be cool to randomize this)
    ap.config(authmode=3, channel=1, password=password)

#Forever loop
while True:
    for i in range (0, len(ssidList)):
        #Choose random MAC Address from list
        randBssid = random.choice(bssidList)
        #Set randByte to unhexlified version of randBSSID
        randByte = ubinascii.unhexlify(randBssid)
        #Set randBSSIDStr to more readable format of randBSSID
        randBssidStr = (':'.join(randBssid[i:i+2] for i in range(0, len(randBssid), 2)))
        #Run the SSID Spawning function
        __ssidSpawn__(ssidList[i],randByte)
        print("Network Spawned - {} ({})".format(ssidList[i], randBssidStr))
        #For every network spawned, blink onboard LED 3 times.
        for i in range (3):
            LED.value(1)
            sleep(0.08)
            LED.value(0)
            sleep(0.08)
        sleep(.3)
